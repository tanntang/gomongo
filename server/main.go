package main

import (
	"context"
	"time"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"log"
	
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson"
)

var database *mongo.Database
var accountCollection *mongo.Collection

type Account struct {
	ID string `bson:"id"`
	Username string `bson:"username"`
	Email string `bson:"email"`
	Point int64 `bson:"point"`
}

//connect db test
func FindDb(w http.ResponseWriter, r *http.Request) {

	dbAddress := os.Getenv("MGDB_SVC_PORT_27017_TCP_ADDR")
	if dbAddress == "" {
		dbAddress = "localhost"
	}

	fmt.Fprintf(w, "MGDB_SVC_PORT_27017_TCP_ADDR = %s\n", dbAddress)

	clientOptions := options.Client().ApplyURI("mongodb://"+ dbAddress +":27017")
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	

	connectContext, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	err = client.Connect(connectContext)
	if err != nil {
		log.Fatal(err)
	}
	defer cancel()
	
	err = client.Ping(connectContext, nil)
	if err != nil {
		log.Fatal(err)
	}

	//list databaseNames
	databaseNames, err := client.ListDatabaseNames(connectContext, bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(w, "Connected to MongoDB ! ---> List of Database Names in Cluster\n%s", databaseNames)
	defer client.Disconnect(connectContext)

	database = client.Database("gmg")
	accountCollection = database.Collection("account")
}

//find one
func FindAccount(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	//w.Header().Set("content-type", "application/json")
	var result Account
	err := accountCollection.FindOne(context.TODO(), bson.D{{"id", id}}).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(w, "account id = %s\n", vars["id"])
	fmt.Fprintln(w, "find document below\n", result)
}
//find many
func FindAccounts(w http.ResponseWriter, r *http.Request) {
	findOptions := options.Find() //setting options first
	//findOptions.SetLimit(3) //limit number of result 

	cursor, err := accountCollection.Find(context.TODO(), bson.D{}, findOptions)
	//cursor, err := account.Find(context.TODO(), bson.D{{"point", bson.D{{"$gt", 300}}}}, findOptions)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close(context.TODO())

	var results []*Account
	fmt.Fprintln(w, "find documents below\n")
	for cursor.Next(context.TODO()) {
		var result Account
		err := cursor.Decode(&result)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &result)
		fmt.Fprintln(w, "\n", result)
	}
}
//insert
func InsertAccount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var account Account
	json.NewDecoder(r.Body).Decode(&account)
	result, err := accountCollection.InsertOne(context.TODO(), account)
	if err != nil {
		log.Fatal(err)
	}
	json.NewEncoder(w).Encode(result)
}

func main() {

	dbAddress := os.Getenv("MGDB_SVC_PORT_27017_TCP_ADDR")
	fmt.Println("MGDB_SVC_PORT_27017_TCP_ADDRR = ", dbAddress)

	if dbAddress == "" {
		dbAddress = "localhost"
	}

	clientOptions := options.Client().ApplyURI("mongodb://"+ dbAddress +":27017")
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	
	connectContext, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	err = client.Connect(connectContext)
	if err != nil {
		log.Fatal(err)
	}
	defer cancel()
	
	err = client.Ping(connectContext, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(connectContext)

	database = client.Database("gmg")
	accountCollection = database.Collection("account")

	
	router := mux.NewRouter()
	router.HandleFunc("/api/accounts", FindAccounts).Methods("GET")
	router.HandleFunc("/api/account/{id}", FindAccount).Methods("GET")
	router.HandleFunc("/api/insert/", InsertAccount).Methods("POST")
	router.Handle("/", http.FileServer(http.Dir("../client")))
	router.HandleFunc("/db", FindDb).Methods("GET")
	http.ListenAndServe(":6060", router)
}